import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import { } from 'googlemaps';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {MyErrorStateMatcher} from '../../util/MyErrorStateMatcher';
import {ErrorStateMatcher} from '@angular/material';
import {AccountType} from '../../util/enums/AccountType';
import {RegistrationService} from './registration.service';
import {UserCreationDTO} from '../../util/dto/UserCreationDTO';
import {HttpErrorResponse} from '@angular/common/http';
import {PasswordPatternValidator} from '../../util/validators/password-pattern.validator';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  @ViewChild('location')
  public searchElementRef: ElementRef;
  public location: google.maps.places.PlaceResult;
  public acceptedLocation: boolean;

  public matcher: ErrorStateMatcher = new MyErrorStateMatcher();
  public registerForm: FormGroup;
  public accountType = AccountType;
  public existedEmail: string = null;

  public registrationSuccessfull: boolean = false;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private formBuilder: FormBuilder,
    private registrationService: RegistrationService,
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang('pl');
    this.constructForm();
  }

  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['(cities)'],
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          this.acceptedLocation = true;
          this.location = autocomplete.getPlace();
          console.log(this.location);
          this.registerForm.get('location').setValue(this.location.formatted_address);
        });
      });
    });

    this.registerForm.get('location').valueChanges.subscribe(control => {
      if (!(this.location && this.acceptedLocation)) {
        this.location = null;
      }
      this.acceptedLocation = false;
    });

  }

  constructForm(): void {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email, this.validateEmailDuplicate.bind(this)])],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      location: ['', Validators.compose([Validators.required, this.validateLocation.bind(this)])],
      password: ['', [Validators.required, new PasswordPatternValidator()]],
      repeatPassword: ['', Validators.compose([Validators.required, this.validatePasswords.bind(this)])],
      accountType: [this.accountType.EMPLOYEE, Validators.required]
    });
  }

  onSubmit() {
    const formValue = this.registerForm.value;

    const place_id: string = this.location.place_id;

    const userCreationDTO: UserCreationDTO = {
      email: formValue.email,
      password: btoa(formValue.password),
      firstName: formValue.name,
      lastName: formValue.surname,
      placeId: place_id,
      accountType: formValue.accountType
    };

    this.registrationService.register(userCreationDTO)
      .subscribe(_ => {
        console.log('success');
        this.registrationSuccessfull = true;
      }, (err: HttpErrorResponse) => {
        if (err.status === 400) {
          this.existedEmail = userCreationDTO.email;
          this.registerForm.get('email').setValue(this.registerForm.get('email').value);
        }
      });
  }

  private validateEmailDuplicate(control: AbstractControl): ValidationErrors | null {
    return this.registerForm && (!this.existedEmail || control.value !== this.existedEmail)
      ? null : { duplicate: true };
  }

  private validatePasswords(control: AbstractControl): ValidationErrors | null {
    return this.registerForm && this.registerForm.get('password').value === this.registerForm.get('repeatPassword').value
      ? null : { passwords: true };
  }

  private validateLocation(control: AbstractControl): ValidationErrors | null {
    return this.registerForm && this.location && this.acceptedLocation
      ? null : { notSelected: true };
  }

  isFormValid() {
    return this.registerForm.valid;
  }
}
