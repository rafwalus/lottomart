import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {UserCreationDTO} from '../../util/dto/UserCreationDTO';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RegistrationService {

  private registerUrl: string = `/api/users`;

  constructor(private http: HttpClient) {}

  register(userCreationDTO: UserCreationDTO): Observable<void> {
    return this.http.post<void>(this.registerUrl, userCreationDTO);
  }

}
