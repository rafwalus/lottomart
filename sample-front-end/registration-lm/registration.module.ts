import {CreateQuestRoutingModule} from '../quests/create-quest-lm/create-quest-routing.module';
import {NgModule} from '@angular/core';
import {DoMyJobModule} from '../../shared/modules/do-my-job.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RegistrationComponent} from './registration.component';
import {RegistrationService} from './registration.service';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: RegistrationComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    DoMyJobModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [
    RegistrationComponent
  ],
  providers: [
    RegistrationService
  ]
})
export class RegistrationModule { }
