import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {DoMyJobModule} from '../../../shared/modules/do-my-job.module';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MyQuestsComponent} from './my-quests.component';
import {MyQuestsService} from './my-quests.service';
import {SearchModule} from '../../../shared/modules/search/search.module';
import {MyQuestRowComponent} from './quest-row/my-quest-row.component';
import {QuestConfirmationModalComponent} from '../../quests/quest-details-lm/quest-confirmation-modal/quest-confirmation-modal.component';
import {QuestReportModalSharedModule} from '../../../shared/modals/quest-report-modal-lm/quest-report-modal-shared.module';

const routes: Routes = [
  {
    path: '',
    component: MyQuestsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    DoMyJobModule,
    ReactiveFormsModule,
    FormsModule,
    SearchModule,
    QuestReportModalSharedModule
  ],
  declarations: [
    MyQuestsComponent,
    MyQuestRowComponent
  ],
  providers: [
    MyQuestsService
  ],
  entryComponents: [
    QuestConfirmationModalComponent
  ]
})
export class MyQuestsModule { }
