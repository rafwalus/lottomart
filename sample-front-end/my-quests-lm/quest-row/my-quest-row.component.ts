import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as distanceInWords from 'date-fns/distance_in_words';
import * as localePL from 'date-fns/locale/pl';
import {CurrencyMap} from '../../../../util/mappers/CurrencyMap';
import {SalaryTypeMap} from '../../../../util/mappers/SalaryTypeMap';
import {QuestType} from '../../../../util/enums/QuestType';
import {QuestDetailsDTO} from '../../../../util/dto/QuestDetailsDTO';
import {MatDialog} from '@angular/material';
import {MyQuestsService} from '../my-quests.service';
import {AccountQuestStatus} from '../../../../util/enums/AccountQuestStatus';
import {AddressUtils} from '../../../../util/address-utils';
import {QuestStatus} from '../../../../util/enums/QuestStatus';
import {MyEntityRowBase} from '../../my-entity-row-base';
import {QuestConfirmationModalComponent} from '../../../quests/quest-details-lm/quest-confirmation-modal/quest-confirmation-modal.component';
import {RatingCreationDTO} from '../../../../util/dto/RatingCreationDTO';
import {QuestReportService} from '../../../../shared/modals/quest-report-modal-lm/quest-report.service';
import {QuestRowEvent} from './quest-row-event';
import 'rxjs/add/operator/delay';



@Component({
  selector: 'app-my-quest-row',
  templateUrl: './quest-row.component.html',
  styleUrls: ['./quest-row.component.scss']
})
export class MyQuestRowComponent extends MyEntityRowBase {

  @Input() public quest: QuestDetailsDTO;
  @Output() actionEmitter: EventEmitter<QuestRowEvent> = new EventEmitter<QuestRowEvent>();

  public questStatus = QuestStatus;
  public questType = QuestType;
  public accountQuestStatus = AccountQuestStatus;

  public historical: boolean = true;

  constructor(
    private service: MyQuestsService,
    private dialog: MatDialog,
    private questReportService: QuestReportService
  ) {
    super();
  }

  openDialogToConfirmQuestSuccess(questDetail: QuestDetailsDTO): void {
    const dialogRef = this.dialog.open(QuestConfirmationModalComponent, {
      width: '500px',
      data: {quest: questDetail}
    });

    dialogRef.afterClosed()
      .subscribe((result: RatingCreationDTO) => {
        if (result) {
          this.actionEmitter.emit(QuestRowEvent.QUEST_REPORT_CONFIRMING_START);
          this.questReportService.confirmQuestFinish(questDetail.id, result)
            .subscribe(_ => {
              this.actionEmitter.emit(QuestRowEvent.QUEST_REPORT_CONFIRMING_END);
            });
        }
      });
  }

}
