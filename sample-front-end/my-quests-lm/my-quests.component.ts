import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {PageDTO} from '../../../util/dto/PageDTO';
import {QuestDetailsDTO} from '../../../util/dto/QuestDetailsDTO';
import {MyQuestsService} from './my-quests.service';
import {QuestStatus} from '../../../util/enums/QuestStatus';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AccountQuestStatus} from '../../../util/enums/AccountQuestStatus';
import 'rxjs/add/operator/switchMap';
import {QuestRowEvent} from './quest-row/quest-row-event';



@Component({
  selector: 'app-publicated-quests',
  templateUrl: './my-quests.component.html',
  styleUrls: ['./my-quests.component.scss']
})
export class MyQuestsComponent implements OnInit {

  public questStatusEnum = QuestStatus;
  public questsCountMap: Map<AccountQuestStatus, number>;
  public questsObservable: Observable<PageDTO<QuestDetailsDTO>>;
  public statusesForm: FormGroup;
  public accountQuestStatus = AccountQuestStatus;


  @ViewChild('assigned') public assignedRef: any;
  public assignedQuests: QuestDetailsDTO[] = [];

  @ViewChild('awaiting') public awaitingRef: any;
  public awaitingQuests: QuestDetailsDTO[] = [];

  @ViewChild('historical') public historicalRef: any;
  public historicalQuests: QuestDetailsDTO[] = [];
  public allHistoricalQuests: QuestDetailsDTO[] = [];

  public searchValue: string = '';
  public loading: boolean = true;
  public initialized: boolean = false;

  constructor(
    private publicatedQuestsService: MyQuestsService,
    private formBuilder: FormBuilder,
  ) {
    this.initStatusesForm();
  }

  ngOnInit() {
    this.countQuestsAndFetch();
  }

  private countQuestsAndFetch(forceRefresh: boolean = false) {
    if (forceRefresh) {
      this.closeAllPanels();
      this.initialized = false;
    }
    this.publicatedQuestsService.getQuestsCount()
      .subscribe(qc => {
        this.questsCountMap = qc;
        this.fetchCorrectQuests();
      });
  }

  private fetchCorrectQuests() {
    if (this.questsCountMap.get(AccountQuestStatus.ASSIGNED) > 0) {
      this.assignedRef.open();
    } else if (this.questsCountMap.get(AccountQuestStatus.AWAITING) > 0) {
      this.awaitingRef.open();
    } else if (this.questsCountMap.get(AccountQuestStatus.HISTORICAL) > 0) {
      this.historicalRef.open();
    } else {
      this.loading = false;
      this.initialized = true;
    }
  }

  getQuestsNumberWithStatus(statuses: QuestStatus[]): number {
    const filtered = this.allHistoricalQuests.filter(q => statuses.includes(q.status));
    return  filtered ? filtered.length : 0;
  }

  initStatusesForm() {
    this.statusesForm = this.formBuilder.group({
      finished: false,
      cancelled: false
    });

    this.statusesForm.valueChanges
      .subscribe(v => {
        console.log(v);
        const wantedStatuses = [];
        if (v.finished) {
          wantedStatuses.push(QuestStatus.FINISHED);
        }
        if (v.cancelled) {
          wantedStatuses.push(QuestStatus.CANCELLED, QuestStatus.EXPIRED);
        }

        this.historicalQuests = wantedStatuses.length === 0 ? this.allHistoricalQuests
          : this.allHistoricalQuests.filter(q => wantedStatuses.includes(q.status));
      });
  }

  handleEvent(event: QuestRowEvent, quest: QuestDetailsDTO) {
    if (event === QuestRowEvent.QUEST_REPORT_CONFIRMING_START) {
      console.log('start')
      this.loading = true;
    } else if (event === QuestRowEvent.QUEST_REPORT_CONFIRMING_END) {
      console.log('end');
      this.countQuestsAndFetch(true);
    }
  }

  getQuestsLength(arr: any[], status: AccountQuestStatus): number {
    return this.searchValue.length > 0 ? arr.length : (this.questsCountMap ? this.questsCountMap.get(status) : 0);
  }

  shouldDisable(arr: any[], status: AccountQuestStatus): boolean {
    return this.initialized && this.getQuestsLength(arr, status) === 0;
  }

  getAssignedQuests(forceRefresh: boolean = false) {
    if (this.assignedQuests.length > 0 && !forceRefresh) {
      return;
    }
    this.loading = true;
    this.publicatedQuestsService.getAssignedQuests(this.searchValue)
      .subscribe(d => {
        this.assignedQuests = d;
        this.loading = false;
        this.initialized = true;
      });
  }

  getAwaitingQuests(forceRefresh: boolean = false) {
    if (this.awaitingQuests.length > 0 && !forceRefresh) {
      return;
    }
    this.loading = true;
    this.publicatedQuestsService.getAwaitingQuests(this.searchValue)
      .subscribe(d => {
        this.awaitingQuests = d;
        this.loading = false;
        this.initialized = true;
      });
  }


  getHistoricalQuests(forceRefresh: boolean = false) {
    if (this.historicalQuests.length > 0 && !forceRefresh) {
      return;
    }
    this.loading = true;
    this.publicatedQuestsService.getHistoricalQuests(this.searchValue)
      .subscribe(d => {
        this.allHistoricalQuests = d;
        this.historicalQuests = d;

        if (this.getQuestsNumberWithStatus([QuestStatus.FINISHED]) > 0) {
          this.statusesForm.setValue({
            'finished': true,
            'cancelled': false
          });
        } else {
          this.statusesForm.setValue({
            'finished': false,
            'cancelled': true
          });
        }


        this.loading = false;
        this.initialized = true;
      });
  }

  public onSearch(value: string): void {
    if (this.anyResults()) {
      this.closeAllPanels();
    }

    this.searchValue = value;
    this.getAssignedQuests(true);
    this.getAwaitingQuests(true);
    this.getHistoricalQuests(true);
  }

  private closeAllPanels() {
    this.assignedRef.close();
    this.awaitingRef.close();
    this.historicalRef.close();
  }

  anyResults(): boolean {
    if (!this.initialized) {
      return true;
    }

    return this.questsCountMap &&
      (
        this.awaitingQuests.length > 0 ||
        this.assignedQuests.length > 0 ||
        this.historicalQuests.length > 0
      );
  }

  hasAnyQuestsInGeneral(): boolean {
    return this.questsCountMap && Array.from(this.questsCountMap.entries()).find(e => e[1] > 0) !== undefined;
  }

  getQuestStatuses() {
    return Object.keys(QuestStatus);
  }

  shouldDisableStatus(statuses: QuestStatus[]): boolean {
    const questsNumberWithStatus = this.getQuestsNumberWithStatus(statuses);
    return questsNumberWithStatus === 0 && this.historicalQuests.length > 0;
  }
}
