import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {PageDTO} from '../../../util/dto/PageDTO';
import {QuestDetailsDTO} from '../../../util/dto/QuestDetailsDTO';
import {AccountQuestStatus} from '../../../util/enums/AccountQuestStatus';
import 'rxjs/add/operator/map';


@Injectable()
export class MyQuestsService {

  constructor(
    private http: HttpClient
  ) {}

  getHistoricalQuests(search: string): Observable<QuestDetailsDTO[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('search', search);

    return this.http.get<QuestDetailsDTO[]>(
      '/api/account/historical-quests',
      {params: httpParams});
  }

  getAwaitingQuests(search: string): Observable<QuestDetailsDTO[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('search', search);

    return this.http.get<QuestDetailsDTO[]>('/api/account/awaiting-quests', {params: httpParams});
  }

  getAssignedQuests(search: string): Observable<QuestDetailsDTO[]> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('search', search);

    return this.http.get<QuestDetailsDTO[]>('/api/account/assigned-quests', {params: httpParams});
  }



  getQuests(statuses: string[]): Observable<PageDTO<QuestDetailsDTO>> {
    let httpParams = new HttpParams();
    statuses.forEach(s => httpParams = httpParams.append('statuses', s));

    return this.http.get<PageDTO<QuestDetailsDTO>>(
      `/api/quests/self`,
      {params: httpParams});
  }

  getQuestsCount(): Observable<Map<AccountQuestStatus, number>> {
    return this.http.get(
      `/api/account/quests/count`
    ).map(d => {
      console.log(d);
      return new Map<AccountQuestStatus, number>(Object.entries(d) as any);
    });
  }
}
