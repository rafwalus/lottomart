import {Component} from '@angular/core';
import {CurrentUserInfoCache} from '../../../util/caches/current-user-info.cache';
import {Router} from '@angular/router';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent {

  public userName: string;
  public userId: number;

  constructor(
    private currentUserInfoCache: CurrentUserInfoCache,
    private router: Router
  ) {
    currentUserInfoCache.getName()
      .then(n => this.userName = n);
    currentUserInfoCache.getId()
      .then(id => this.userId = id);
  }

  goToUserProfile() {
    if (this.userId) {
      this.router.navigate([`/app/users/${this.userId}`]);
    }
  }

}
