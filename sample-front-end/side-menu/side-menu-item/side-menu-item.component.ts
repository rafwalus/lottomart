import {Component, Input, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import 'rxjs/add/operator/filter';


@Component({
  selector: 'app-side-menu-item',
  templateUrl: './side-menu-item.component.html',
  styleUrls: ['./side-menu-item.component.scss']
})
export class SideMenuItemComponent implements OnInit {

  @Input() primary: boolean = false;
  @Input() path: string;
  @Input() label: string;
  @Input() icon: string;
  public active: boolean;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.active = this.path === this.router.url;
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe((e: NavigationEnd) => this.active = this.path === e.urlAfterRedirects);
  }

  getColor(): string {
    return this.primary ? 'primary' : '';
  }
}
