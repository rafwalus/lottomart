package pl.domyjob.app.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import pl.domyjob.app.enums.AccountQuestsStatus;
import pl.domyjob.app.model.users.User;
import pl.domyjob.app.service.QuestService;
import pl.domyjob.app.web.dto.quest.QuestCreationDTO;
import pl.domyjob.app.web.dto.quest.QuestDetailsDTO;
import pl.domyjob.app.web.dto.quest.QuestFilterDTO;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
public class QuestController {

    private QuestService questService;

    @Autowired
    public QuestController(QuestService questService) {
        this.questService = questService;
    }


    @PostMapping(value = "/api/quests", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public QuestDetailsDTO createQuest(
            @RequestBody @Valid QuestCreationDTO offerDTO,
            @AuthenticationPrincipal User user,
            Locale locale) {
        return questService.createQuest(offerDTO, user, locale);
    }

    @PostMapping(value = "/api/quests/find", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<QuestDetailsDTO> getQuests(@RequestBody @Valid QuestFilterDTO filterDTO, Locale locale, @AuthenticationPrincipal User user) {
        return questService.findQuests(filterDTO, locale, user);
    }


    @GetMapping(value = "/api/account/assigned-quests")
    public List<QuestDetailsDTO> getAssignedUserQuests(
            @AuthenticationPrincipal User user,
            @RequestParam("search") Optional<String> search,
            Locale locale) {
        return questService.getAccountQuests(user, search, locale, AccountQuestsStatus.ASSIGNED);
    }

    @GetMapping(value = "/api/account/awaiting-quests")
    public List<QuestDetailsDTO> getAwaitingUserQuests(
            @AuthenticationPrincipal User user,
            @RequestParam("search") Optional<String> search,
            Locale locale) {
        return questService.getAccountQuests(user, search, locale, AccountQuestsStatus.AWAITING);
    }

    @GetMapping(value = "/api/account/historical-quests")
    public List<QuestDetailsDTO> getHistoricalUserQuests(
            @AuthenticationPrincipal User user,
            @RequestParam("search") Optional<String> search,
            Locale locale) {
        return questService.getAccountQuests(user, search, locale, AccountQuestsStatus.HISTORICAL);
    }


    @GetMapping(value = "/api/account/quests/count")
    public Map<AccountQuestsStatus, Integer> getCurrentUserQuestsCount(@AuthenticationPrincipal User user) {
        return questService.getCurrentUserQuestsCount(user);
    }

    @GetMapping(value = "/api/quests/{questId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public QuestDetailsDTO getQuest(@PathVariable("questId") Long id, Locale locale, @AuthenticationPrincipal User user) {
        return questService.getQuest(id, locale, user);
    }

    @GetMapping(value = "/api/users/{userId}/quests")
    public List<QuestDetailsDTO> getQuestsByOwner(@PathVariable("userId") Long id,
                                                  Locale locale) {
        return questService.getPublishedQuestsByUser(id, locale);
    }

    @PostMapping(value = "/api/quests/{questId}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelQuest(@PathVariable("questId") Long questId, @AuthenticationPrincipal User user) {
        log.info("[CANCELING QUEST]");
        questService.cancelQuest(questId, user);
    }

}
