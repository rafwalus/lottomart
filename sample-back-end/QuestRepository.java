package pl.domyjob.app.repository;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import pl.domyjob.app.enums.QuestStatus;
import pl.domyjob.app.model.quests.Quest;
import pl.domyjob.app.model.users.User;
import pl.domyjob.app.web.dto.quest.QuestsCount;

import javax.persistence.LockModeType;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


public interface QuestRepository extends PagingAndSortingRepository<Quest, Long>, QueryDslPredicateExecutor<Quest> {

    Optional<Quest> findById(Long id);

    List<Quest> findByOwnerAndStatus(User quester, QuestStatus questStatus);

    List<Quest> findByOwnerAndStatusIn(User quester, List<QuestStatus> questStatuses);

    @Query("SELECT new pl.domyjob.app.web.dto.quest.QuestsCount(q.status, count(q)) " +
            "FROM Quest q " +
            "WHERE q.owner = :owner " +
            "GROUP BY q.status")
    List<QuestsCount> getQuestsCount(@Param("owner") User owner);

    @Query("SELECT q FROM Quest q WHERE :dateTime >= q.expirationDate AND q.status = :status")
    List<Quest> findIdsOfQuestsByExpirationDateAndStatus(
            @Param("dateTime") LocalDateTime dateTime,
            @Param("status") QuestStatus status);

    @Modifying
    @Query("update Quest q set q.views = q.views + 1 where q = :quest")
    void incrementViews(@Param("quest") Quest quest);
}
