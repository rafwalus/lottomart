package pl.domyjob.emails;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import pl.domyjob.emails.structures.EmailStructure;

import javax.mail.internet.MimeMessage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class EmailSender {

    private final JavaMailSender emailSender;
    private final Scheduler mailsScheduler = Schedulers.computation();
    private final Integer retriesNumber;
    private final Integer retriesInterval;


    @Autowired
    public EmailSender(
            JavaMailSender emailSender,
            @Value("${app.emails.retries.number}") Integer retriesNumber,
            @Value("${app.emails.retries.interval}") Integer retriesInterval) {
        this.retriesNumber = retriesNumber;
        this.emailSender = emailSender;
        this.retriesInterval = retriesInterval;
    }


    public Completable doSend(EmailStructure emailStructure) {
        return Completable.fromAction(() -> {
                final MimeMessage mimeMessage = emailSender.createMimeMessage();
                final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8");
                message.setSubject(emailStructure.getSubject());
                message.setTo(emailStructure.getTo());
                message.setText(emailStructure.getText(), true);
                emailSender.send(message.getMimeMessage());
        })
                .retryWhen(attempts -> handleRetries(emailStructure, attempts))
                .doOnError(t -> log.error(emailStructure.getLogError(), t))
                .subscribeOn(mailsScheduler);
    }

    private Publisher<?> handleRetries(EmailStructure emailStructure, Flowable<Throwable> attempts) {
        AtomicInteger counter = new AtomicInteger(1);
        return attempts.flatMapSingle(a -> {
            log.error(emailStructure.getLogRetry(counter.intValue()), a);
            return counter.getAndIncrement() >= retriesNumber ? Single.error(a) : Single.timer(retriesInterval, TimeUnit.SECONDS);
        });
    }

}