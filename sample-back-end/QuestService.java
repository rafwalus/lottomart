package pl.domyjob.app.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;
import pl.domyjob.app.enums.AccountQuestsStatus;
import pl.domyjob.app.enums.AddressType;
import pl.domyjob.app.model.Application;
import pl.domyjob.app.model.users.User;
import pl.domyjob.app.repository.CategoryRepository;
import pl.domyjob.app.repository.QuestRepository;
import pl.domyjob.app.service.builder.QuestsFilterPredicateBuilder;
import pl.domyjob.app.service.mapper.AddressMapper;
import pl.domyjob.app.service.exceptions.principal.UserNotOwnerOfQuestException;
import pl.domyjob.app.service.exceptions.quest.QuestCannotBeCancelledException;
import pl.domyjob.app.service.exceptions.quest.QuestNotFoundException;
import pl.domyjob.app.service.mapper.QuestMapper;
import pl.domyjob.app.web.dto.quest.QuestCreationDTO;
import pl.domyjob.app.web.dto.quest.QuestDetailsDTO;
import pl.domyjob.app.web.dto.quest.QuestFilterDTO;
import pl.domyjob.app.web.dto.quest.QuestsCount;
import pl.domyjob.app.model.Category;
import pl.domyjob.app.enums.QuestStatus;
import pl.domyjob.app.model.quests.Quest;
import pl.domyjob.app.service.exceptions.category.CategoryNotFoundException;
import pl.domyjob.events.enums.EventType;
import pl.domyjob.events.service.EventService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class QuestService {

    private final QuestRepository questRepository;
    private final CategoryRepository categoryRepository;
    private final EventService eventService;
    private final AddressMapper addressMapper;
    private final QuestsFilterPredicateBuilder questsFilterPredicateBuilder;
    private final QuestMapper questMapper;

    @Autowired
    public QuestService(QuestRepository questRepository,
                        CategoryRepository categoryRepository,
                        EventService eventService,
                        AddressMapper addressMapper, QuestMapper questMapper) {
        this.questRepository = questRepository;
        this.categoryRepository = categoryRepository;
        this.eventService = eventService;
        this.addressMapper = addressMapper;
        this.questMapper = questMapper;
        this.questsFilterPredicateBuilder = new QuestsFilterPredicateBuilder();
    }

    public QuestDetailsDTO createQuest(QuestCreationDTO questCreationDTO, User user, Locale locale) {
        Category category = doGetCategoryByName(questCreationDTO.getCategoryName());
        Quest quest = questCreationDTO.toEntity();

        quest.setCategory(category);
        if (questCreationDTO.getPlaceId() != null) {
            quest.setAddress(addressMapper.createEntity(AddressType.STREET, questCreationDTO.getPlaceId()));
        }
        quest.setPublicationDate(LocalDateTime.now());
        quest.setStatus(QuestStatus.PUBLISHED);
        quest.setOwner(user);
        quest.setApplications(Collections.emptyList());
        questRepository.save(quest);

        return questMapper.toDetailsDTO(quest, Optional.empty(), true, locale);
    }

    public Page<QuestDetailsDTO> findQuests(QuestFilterDTO filterDTO, Locale locale, User user) {
        Pageable pageable = createPageRequest(filterDTO.getPaging());

        return questsFilterPredicateBuilder.build(filterDTO, user)
                .map(predicate -> questRepository.findAll(predicate, pageable))
                .orElseGet(() -> questRepository.findAll(pageable))
                .map(quest -> {
                    Optional<Application> application = quest.getApplications()
                            .stream()
                            .filter(a -> a.getApplicant().equals(user))
                            .findAny();

                    return questMapper.toDetailsDTO(quest, application, quest.getOwner().equals(user), locale);
                });
    }

    @Transactional
    public QuestDetailsDTO getQuest(Long questId, Locale locale, User user) {
        Quest quest = doGetQuestById(questId);

        if (shouldIncrementViews(user, quest)) {
            questRepository.incrementViews(quest);
        }

        Optional<Application> userApplication = quest.getApplications()
                .stream()
                .filter(a -> a.getApplicant().getId().equals(user.getId()))
                .findFirst();

        boolean questOwner = quest.getOwner().getId().equals(user.getId());

        return questMapper.toDetailsDTO(quest, userApplication, questOwner, locale);
    }

    private boolean shouldIncrementViews(User user, Quest quest) {
        return !quest.getOwner().getId().equals(user.getId());
    }


    public void cancelQuest(Long questId, User user) {
        Quest quest = doGetQuestById(questId);

        if (!user.getId().equals(quest.getOwner().getId())) {
            throw new UserNotOwnerOfQuestException();
        }

        if (QuestStatus.PUBLISHED != quest.getStatus()) {
            throw new QuestCannotBeCancelledException(quest.getStatus());
        }

        quest.setStatus(QuestStatus.CANCELLED);
        questRepository.save(quest);
        eventService.handle(EventType.CANCEL_QUEST, quest);
    }


    public List<QuestDetailsDTO> getAccountQuests(User user,
                                                  Optional<String> search,
                                                  Locale locale,
                                                  AccountQuestsStatus type) {

        return questRepository.findByOwnerAndStatusIn(user, getStatusesByAccountQuestsType(type))
                .stream()
                .filter(q -> meetsSearchCriteria(search, q))
                .map(q -> questMapper.toDetailsDTO(q, Optional.empty(), true, locale))
                .collect(Collectors.toList());
    }

    private boolean meetsSearchCriteria(Optional<String> search, Quest q) {
        return search
                .filter(s -> !StringUtils.isEmpty(s))
                .map(s -> q.getName().toLowerCase().contains(s) ||
                    q.getId().toString().contains(s) ||
                    q.getAddress().map( address -> address.getAddressComponents().stream().anyMatch(ac -> ac.getValue().toLowerCase().contains(s))).orElse(true)
                ).orElse(true);
    }

    private List<QuestStatus> getStatusesByAccountQuestsType(AccountQuestsStatus accountQuestsStatus) {
        switch (accountQuestsStatus) {
            case ASSIGNED: return Collections.singletonList(QuestStatus.ASSIGNED);
            case AWAITING: return Collections.singletonList(QuestStatus.PUBLISHED);
            case HISTORICAL: return Arrays.asList(QuestStatus.CANCELLED, QuestStatus.EXPIRED, QuestStatus.FINISHED);
            default: throw new RuntimeException();
        }
    }

    public Map<AccountQuestsStatus, Integer> getCurrentUserQuestsCount(User user) {
        Map<QuestStatus, Long> questsCount = questRepository
                .getQuestsCount(user)
                .stream()
                .collect(Collectors.toMap(QuestsCount::getStatus, QuestsCount::getCount));

        return Arrays.stream(AccountQuestsStatus.values())
                .collect(Collectors.toMap(
                        Function.identity(),
                        aqt -> aqt.getStatuses().stream().mapToInt(qs -> Optional.ofNullable(questsCount.get(qs)).orElse(0L).intValue()).sum())
                );

    }

    public List<QuestDetailsDTO> getPublishedQuestsByUser(Long id, Locale locale) {
        return questRepository.findByOwnerAndStatus(User.builder().id(id).build(), QuestStatus.PUBLISHED)
                .stream()
                .map(q -> questMapper.toDetailsDTO(q, Optional.empty(), false, locale))
                .collect(Collectors.toList());
    }

    public void expireQuests() {
        List<Quest> quests = questRepository.findIdsOfQuestsByExpirationDateAndStatus(LocalDateTime.now(), QuestStatus.PUBLISHED);
        quests.forEach(q -> {
            q.setStatus(QuestStatus.EXPIRED);
            try {
                questRepository.save(q);
                eventService.handle(EventType.EXPIRE_QUEST, q);
            } catch (ObjectOptimisticLockingFailureException e) {
                log.error(String.format("Could not mark quest [id: %s] as expired - optimistic lock exception", q.getId()), e);
            }
        });
    }

    private Quest doGetQuestById(Long questId) {
        return questRepository.findById(questId)
                .orElseThrow(() -> new QuestNotFoundException(questId));
    }

    private Category doGetCategoryByName(String name) {
        return categoryRepository.findByName(name)
                .orElseThrow(() -> new CategoryNotFoundException(name));
    }

    private Pageable createPageRequest(QuestFilterDTO.Paging paging) {
        Sort sort = paging.getSorting()
                .map(s -> new Sort(s.getSortDirection(), s.getName()))
                .orElseGet(() -> new Sort(Sort.Direction.DESC, "publicationDate"));

        return new PageRequest(
                paging.getPageNumber(),
                paging.getPageSize(),
                sort);
    }

}
