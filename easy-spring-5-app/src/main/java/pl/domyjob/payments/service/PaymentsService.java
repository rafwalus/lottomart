package pl.domyjob.payments.service;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.domyjob.payments.controller.dto.CreatePaymentDTO;
import pl.domyjob.payments.model.Payment;
import pl.domyjob.payments.repository.PaymentsRepo;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class PaymentsService {

    private final PaymentsRepo paymentsRepo;
    private final ModelMapper modelMapper;

    public PaymentsService(PaymentsRepo paymentsRepo, ModelMapper modelMapper) {
        this.paymentsRepo = paymentsRepo;
        this.modelMapper = modelMapper;
    }

    public Mono<Payment> getPaymentById(String id) {
        return paymentsRepo.findById(id);
    }

    public Mono<Payment> createPayment(CreatePaymentDTO createPaymentDTO) {
        Payment payment = modelMapper.map(createPaymentDTO, Payment.class);
        return paymentsRepo.save(payment);
    }
}
