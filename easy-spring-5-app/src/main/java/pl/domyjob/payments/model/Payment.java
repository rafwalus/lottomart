package pl.domyjob.payments.model;

import lombok.Getter;
import lombok.Setter;
import java.util.UUID;

@Setter
@Getter
public class Payment {

    private String id = UUID.randomUUID().toString();

    private Long userId;
}