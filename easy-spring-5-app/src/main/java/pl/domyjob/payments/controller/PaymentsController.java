package pl.domyjob.payments.controller;

import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.domyjob.payments.controller.dto.CreatePaymentDTO;
import pl.domyjob.payments.controller.dto.PaymentDTO;
import pl.domyjob.payments.service.PaymentsService;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import java.net.URI;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RestController
@RequestMapping("/api/payments")
public class PaymentsController {

    private final PaymentsService paymentsService;
    private final ModelMapper modelMapper;

    public PaymentsController(PaymentsService paymentsService, ModelMapper modelMapper) {
        this.paymentsService = paymentsService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_UTF8_VALUE)
    public Mono<ResponseEntity<PaymentDTO>> getPaymentById(@PathVariable String id) {
        return paymentsService.getPaymentById(id)
                .map(p -> modelMapper.map(p, PaymentDTO.class))
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public Mono<ResponseEntity<PaymentDTO>> createPayment(@RequestBody @Valid Mono<CreatePaymentDTO> createPaymentDTO) {
        return createPaymentDTO
                .flatMap(paymentsService::createPayment)
                .map(p -> modelMapper.map(p, PaymentDTO.class))
                .map(dto -> ResponseEntity
                        .created(URI.create("/api/payments/" + dto.getId()))
                        .body(dto)
                );
    }
}
