package pl.domyjob.payments.controller.dto;

import javax.validation.constraints.Max;

public class CreatePaymentDTO {

    @Max(12)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
