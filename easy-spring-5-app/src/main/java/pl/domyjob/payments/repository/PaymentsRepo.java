package pl.domyjob.payments.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import pl.domyjob.payments.model.Payment;

public interface PaymentsRepo extends ReactiveMongoRepository<Payment, String> {}
